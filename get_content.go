package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/golang/freetype"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"
)

type File struct {
	Filetype  string
	Url       string
	Extension string
}

type Result struct {
	Score  int64
	Title  string
	Author string
	Prize  string
	Files  []*File
}

type Competition struct {
	Competition string
	Results     []*Result
}

type GetResults struct {
	Succsess bool
	Action   string
	Data     *Competition
}

// The competitions
// TODO: Change to downloading correct order from Unicorn
var contests = []int{
14, //Handdrawn
//Multidisiplin
//=============
25, //Demo
56, //VR-Demo
55, //Google Blocks
27, //Spillutvikling
28, //Fast GameJam
29, //Dragons Den
30, //Hacking
//Programmering
//=============
22, //AI
23, //Useless utility
24, //Small html
26, //TGPC
//Musikk
//======
7, //Themed Music
8, //Soundtrack
6, //Fast Music
5, //Freestyle
//Video
//=====
18, //Freestyle video
11, //10 Second movie
17, //Themed video (intro)
//Grafikk
//=======
12, //Fast MS Paint
57, //Themed: Fast Manipulation
13, //Themed: Fast Graphics
58, //Themed: Fast Concept visualization
14, //Handdrawn
15, //Rendered
16, //Themd photo
}

// We don't want this content
var contentBlacklist = map[int]bool{
	25: true,
}

func DownloadFile(location string, filename string, url string) (err error) {
	// Create the file
	os.MkdirAll(location, os.ModePerm)

	out, err := os.Create(location + "/" + filename)
	if err != nil {
		return err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Check server response
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status: %s", resp.Status)
	}

	// Writer the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	return nil
}

func ReqGetResults(n int) *GetResults {
	client := &http.Client{
		Timeout: time.Second * 10,
	}
	req, err := http.NewRequest("GET", "https://unicorn.gathering.org/api/beamer/results/"+strconv.Itoa(n), nil)

	resp, err := client.Do(req)
	if err != nil {
		return nil
	}
	// Don't forget to close the response body
	defer resp.Body.Close()

	res := &GetResults{}

	err = json.NewDecoder(resp.Body).Decode(&res)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return res
}

func GetSortedWinners(entries []*Result) []*Result {
	var wrongEntries []*Result
	for _, entry := range entries {
		if entry.Prize == "" {
			break
		}
		wrongEntries = append(wrongEntries, entry)
	}
	ret := make([]*Result, len(wrongEntries))
	for j, n := len(wrongEntries), 0; j != 0; j, n = j-1, n+1 {
		ret[n] = wrongEntries[j-1]
	}
	return ret
}

var (
	dpi      = flag.Float64("dpi", 72, "screen resolution in Dots Per Inch")
	fontfile = flag.String("fontfile", "Helvetica-Condensed-Black.ttf", "filename of the ttf font")
	hinting  = flag.String("hinting", "full", "none | full")
	size     = flag.Float64("size", 115, "font size in points")
	spacing  = flag.Float64("spacing", 1.5, "line spacing (e.g. 2 means double spaced)")

	font1 = string("Helvetica-Black.ttf")
	font2 = string("Helvetica-Condensed-Black.ttf")

	t_compo  = flag.String("c", "GNU/Freestyle", "Compo name/That was:")
	t_res    = flag.String("r", "Results", "Compo name/That was:")
	t_num    = flag.String("n", "#", "The number as a string")
	t_group  = flag.String("g", "TryHARDs", "Group/Author")
	t_msg    = flag.String("m", "…", "Message for the Crew")
	t_name   = flag.String("a", "Bob (2345)", "Message for the Crew")
	t_foot_l = string("Fast Themes Graphics")

	t_foot_r = string("Vote at competitions.gathering.org")

	name_i = ""

	res_w  = 1920
	res_h  = 1080
	border = 8
	res    = res_w * res_h
	count  = uint32(0)

	// Font sizes. Should be a derived from res_w/res_h
	fsize_rect_res   = float64(111)
	fsize_rect_compo = float64(111)
	fsize_rect_num   = float64(300)
	fsize_rect_name  = float64(150)
	fsize_rect_title = float64(71)
	fsize_rect_group = float64(86)
	fsize_rect_msg   = float64(60)
	fsize_rect_footl = float64(100)
	fsize_rect_footr = float64(36)

	// Colors
	white_a4    = color.RGBA{255, 255, 255, 172}
	white_a5    = color.RGBA{255, 255, 255, 255}
	black_a4    = color.RGBA{100, 100, 100, 172}
	black_a5    = color.RGBA{0, 0, 0, 214}
	yellow_1    = color.RGBA{255, 222, 0, 255}
	turquoise_1 = color.RGBA{64, 224, 208, 255}
)

func padNumberWithZero(value uint32) string {
    return fmt.Sprintf("%03d", value)
}

func GenerateSlide(comp *Competition) {
	// fmt.Printf("Loading fontfile %q\n", *fontfile)
	// fmt.Printf("Loading fontfile %q\n", test)
	fmt.Println(comp.Competition)

	// Hardcoded font 1
	// b1, err := ioutil.ReadFile(font1)
	// if err != nil {
	// 	log.Println(err)
	// 	return
	// }
	// f1, err := truetype.Parse(b1)
	// if err != nil {
	// 	log.Println("b1")
	// 	log.Println(err)
	// 	return
	// }

	// Hardcoded font 2
	b2, err := ioutil.ReadFile(font2)
	if err != nil {
		log.Println("font2")
		log.Println(err)
		return
	}
	f2, err := truetype.Parse(b2)
	if err != nil {
		log.Println("b2")
		log.Println(err)
		return
	}

	// // Generic font
	b, err := ioutil.ReadFile(*fontfile)
	if err != nil {
		log.Println(err)
		return
	}
	f, err := truetype.Parse(b)
	if err != nil {
		log.Println(err)
		return
	}

	// Freetype context
	// size_1 := float64((4*res_h/18 - 4*border) / 2)
	size_1 := float64(280)
	fmt.Printf("%+v\n", size_1)
	black, white, bg := image.Black, image.White, image.Transparent

	rgba := image.NewRGBA(image.Rect(0, 0, res_w, res_h))
	draw.Draw(rgba, rgba.Bounds(), bg, image.ZP, draw.Src)
	c := freetype.NewContext()
	c.SetDPI(*dpi)
	c.SetFont(f)
	c.SetFontSize(*size)
	c.SetFontSize(size_1)
	c.SetClip(rgba.Bounds())
	c.SetDst(rgba)
	c.SetSrc(black)
	c.SetSrc(white)

	switch *hinting {
	default:
		c.SetHinting(font.HintingNone)
	case "full":
		c.SetHinting(font.HintingFull)
	}


	// Generate intro slide
	// Boxes
	rect_compo := image.Rect(border, border, res_w - border, 3*res_h/18 - border)
	rect_place := image.Rect(border, 3*res_h/18 + border, res_w/4 - border, 9*res_h/18 - border)
	rect_res   := image.Rect(res_w/4 + border, 3*res_h/18 + border, res_w - border, 9*res_h/18 - border)
	rect_name  := image.Rect(border, 9*res_h/18 + border, res_w - border, res_h - border)

	draw.Draw(rgba, rgba.Bounds(), bg, image.ZP, draw.Src)
	draw.Draw(rgba, rect_compo, &image.Uniform{black_a5}, image.ZP, draw.Src)
	//draw.Draw(rgba, rect_place, &image.Uniform{yellow_1}, image.ZP, draw.Src)
	draw.Draw(rgba, rect_place, &image.Uniform{turquoise_1}, image.ZP, draw.Src)
	draw.Draw(rgba, rect_res, &image.Uniform{white}, image.ZP, draw.Src)
	draw.Draw(rgba, rect_name, &image.Uniform{black_a5}, image.ZP, draw.Src)


	// Fonts
	opts := truetype.Options{size_1, *dpi, font.HintingFull, 0, 64, 1}
	opts.Size = size_1


	// Calculate some mids
	opts.Size = fsize_rect_num
	face_num := truetype.NewFace(f2, &opts)
	width_num_f := font.MeasureString(face_num, *t_num)
	width_num := width_num_f.Round()


	// Draw stuff
	pt_res := freetype.Pt(int(0.5 * fsize_rect_res), int(3*res_h/36) + int(0.377 * fsize_rect_res))
	c.SetSrc(&image.Uniform{yellow_1})
	c.SetFont(f2)
	c.SetFontSize(fsize_rect_res)
	c.DrawString(*t_res, pt_res)

	pt_num := freetype.Pt((res_w/4 - width_num)/2, 6*res_h/18 + int(0.377 * fsize_rect_num))
	c.SetSrc(black)
	c.SetFont(f2)
	c.SetFontSize(fsize_rect_num)
	c.DrawString(*t_num, pt_num)

	pt_compo := freetype.Pt(res_w/4 + int(0.5 * fsize_rect_compo), 6*res_h/18 + int(0.377 * fsize_rect_compo))
	c.SetSrc(black)
	c.SetFont(f2)
	c.SetFontSize(fsize_rect_compo)
	c.DrawString(comp.Competition, pt_compo)


	// Save that RGBA image to disk.
	count++
	outFile, err := os.Create("slides/"+ padNumberWithZero(count) +".png")
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	defer outFile.Close()
	bf := bufio.NewWriter(outFile)
	err = png.Encode(bf, rgba)
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	err = bf.Flush()
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	fmt.Println("Wrote out.png OK.")

	// Generate slides for the winners
	// Iterate over the first 5 places
	for i := 4; i >= 0; i--{
		if (len(comp.Results) > i && comp.Results[i].Author != "") {
			if comp.Results[i].Prize == "" {
				fmt.Println("No  prize for " + strconv.Itoa(i+1) + ".")
			} else {
				// Reset image with new boxes
				draw.Draw(rgba, rgba.Bounds(), bg, image.ZP, draw.Src)
				draw.Draw(rgba, rect_compo, &image.Uniform{black_a5}, image.ZP, draw.Src)
				draw.Draw(rgba, rect_place, &image.Uniform{yellow_1}, image.ZP, draw.Src)
				draw.Draw(rgba, rect_res, &image.Uniform{white}, image.ZP, draw.Src)
				draw.Draw(rgba, rect_name, &image.Uniform{black_a5}, image.ZP, draw.Src)

				// Calculate some mids
				opts.Size = fsize_rect_num
				face_num_i := truetype.NewFace(f2, &opts)
				width_num_i_f := font.MeasureString(face_num_i, strconv.Itoa(i+1))
				width_num_i := width_num_i_f.Round()

				if comp.Results[i].Score > 0 {
					name_i = comp.Results[i].Author + " (" + strconv.Itoa(int(comp.Results[i].Score)) + ")"
				} else {
					name_i = comp.Results[i].Author
				}

				// Calculate centering of name
				opts.Size = fsize_rect_name
				face_name_i := truetype.NewFace(f2, &opts)
				width_name_i_f := font.MeasureString(face_name_i, name_i)
				width_name_i := width_name_i_f.Round()

				// Shrink to fit if width is too wide
				if width_name_i > int(0.9*float64(res_w)) {
					fsize_rect_name *= float64(res_w)/float64(width_name_i) * 0.9
					opts.Size = fsize_rect_name
					face_name_i = truetype.NewFace(f2, &opts)
					width_name_i_f = font.MeasureString(face_name_i, name_i)
					width_name_i = width_name_i_f.Round()
				}

				// Draw text
				pti_res := freetype.Pt(int(0.5 * fsize_rect_res), int(3*res_h/36) + int(0.377 * fsize_rect_res))
				c.SetSrc(&image.Uniform{yellow_1})
				c.SetFont(f2)
				c.SetFontSize(fsize_rect_res)
				c.DrawString(*t_res, pti_res)

				pti_num := freetype.Pt((res_w/4 - width_num_i)/2, 6*res_h/18 + int(0.377 * fsize_rect_num))
				c.SetSrc(black)
				c.SetFont(f2)
				c.SetFontSize(fsize_rect_num)
				c.DrawString(strconv.Itoa(i+1), pti_num)

				pti_compo := freetype.Pt(res_w/4 + int(0.5 * fsize_rect_compo), 6*res_h/18 + int(0.377 * fsize_rect_compo))
				c.SetSrc(black)
				c.SetFont(f2)
				c.SetFontSize(fsize_rect_compo)
				c.DrawString(comp.Competition, pti_compo)

				pti_name := freetype.Pt((res_w - width_name_i)/2, 27*res_h/36 + int(0.377 * fsize_rect_name))
				c.SetSrc(&image.Uniform{yellow_1})
				c.SetFont(f2)
				c.SetFontSize(fsize_rect_name)
				c.DrawString(name_i, pti_name)

				// Save slide
				count++
				outi, err := os.Create("slides/"+ padNumberWithZero(count) +".png")
				if err != nil {
					log.Println(err)
					os.Exit(1)
				}
				defer outi.Close()
				bfi := bufio.NewWriter(outi)
				err = png.Encode(bfi, rgba)
				if err != nil {
					log.Println(err)
					os.Exit(1)
				}
				err = bfi.Flush()
				if err != nil {
					log.Println(err)
					os.Exit(1)
				}
				fmt.Println("Wrote out.png OK.")

				// Download
				switch comp.Results[i].Files[0].Filetype {
				default:
					continue
				case "picture":
					if i >= 3 {
						continue
					}
				case "music":
					if i >= 1 {
						continue
					}
				}
				count++
				DownloadFile("slides/", padNumberWithZero(count) + "." + comp.Results[i].Files[0].Extension, comp.Results[i].Files[0].Url)
			}
		}
	}
}

func main() {
	// Parse flags
	flag.Parse()

	// Ensure slide directory
	slidepath := filepath.Join(".", "slides")
	if _, err := os.Stat(slidepath); os.IsNotExist(err) {
		os.MkdirAll(slidepath, os.ModePerm)
	}

	// Iterate over compos
	for _, v := range contests {
		l := ReqGetResults(v)
		if l != nil && l.Data.Results != nil {
//			sortedEntries := GetSortedWinners(l.Data.Results)
			GenerateSlide(l.Data)
			//os.Exit(0)
//			for _, entry := range sortedEntries {
//				fmt.Println(entry.Prize)
//			}
			// for _, entry := range l.Data.Results {

			// generate slide here

			// If we are in a blacklisted contest, move along
			// if val, ok := contentBlacklist[v]; ok {
			// 	continue
			// }

			// for _, file := range entry.Files {
			// 	DownloadFile("./output", entry.Title+"."+file.Extension, file.Url)
			// 	os.Exit(0)
			// }

			// }
		}
	}
}
